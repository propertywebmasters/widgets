<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use App\Models\Widget;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        // Example Flash MSG...
        // flash('Logged in!')->success();

        // Get User Company Details...
        $company = Company::find(Auth::user()->company_id);

        if(empty($company->websites))
        {
            // Redirect to Create a Website (As a Widget needs to be attached to this)...
            flash('It looks you haven\t created a website, please create one to add a Widget')->info();
            return redirect('admin/website/create');
        }
        else
        {
            $widgets = Widget::whereIn('website_id', $company->websites->pluck('website_id')->toArray())->get();

            return view('admin.index',
                [
                    'company' => $company,
                    'widgets' => $widgets
                ]
            );
        }
    }
}
