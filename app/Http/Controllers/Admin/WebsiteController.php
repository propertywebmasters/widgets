<?php

namespace App\Http\Controllers\Admin;

use App\Models\CompanyWebsite;
use App\Models\Website;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WebsiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.website.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(),
            [
                'website_name' => 'required',
                'website_url' => 'required'
            ]
        )->validate();

        // Create New Website...
        $website = new Website;
        $website->website_name = $request->input('website_name');
        $website->website_url = $request->input('website_url');
        $website->save();

        // Also create CompanyWebsite Record...
        $company_website = new CompanyWebsite;
        $company_website->company_id = Auth::user()->company_id;
        $company_website->website_id = $website->website_id;
        $company_website->save();

        // Done - Redirect to Dashboard...
        flash('Website Created - Now try adding some Widgets!');
        return redirect('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.website.edit',
            [
                'website' => Website::find($id)
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(),
            [
                'website_name' => 'required',
                'website_url' => 'required'
            ]
        )->validate();

        $website = Website::find($id);
        $website->website_name = $request->input('website_name');
        $website->website_url = $request->input('website_url');
        $website->save();

        flash('Website Updated')->success();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
