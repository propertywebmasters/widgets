<?php

namespace App\Http\Controllers\Admin;

use App\Models\Widget;
use App\Models\WidgetAttribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WidgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.widget.create',
            [
                'websites' => Auth::user()->company->websites,
                'method'   => 'create',
                'types'    => config('widget_types.types')
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get Widget Type...
        $widget_type = $request->input('widget_type');

        // Validate (Per Widget Type) - Maybe put these in a Config File...
        if($widget_type == 'overlay')
        {
            Validator::make($request->all(),
                [
                    'widget_name'               => 'required',
                    'widget_type'               => 'required',
                    'attributes.title'          => 'required',
                    'attributes.sub_title'      => 'required',
                    'attributes.price'          => 'required',
                    'attributes.details_link'   => 'required'
                ]
            )->validate();
        }


        // First Create The Initial Widget Data (Name / Type)...
        $widget = new Widget;
        $widget->widget_name = $request->input('widget_name');
        $widget->widget_type = $request->input('widget_type');
        $widget->website_id = $request->input('website_id');
        $widget->save();

        // Now Create The Attributes....
        foreach($request->input('attributes') as $k => $v)
        {
            $attribute = new WidgetAttribute;
            $attribute->widget_id = $widget->widget_id;
            $attribute->widget_attribute_key = $k;
            $attribute->widget_attribute_value = $v;
            $attribute->save();
        }

        flash('Widget created')->success();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd("Show!");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.widget.edit',
            [
                'widget' => Widget::find($id),
                'method' => 'edit'
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $widget = Widget::find($id);
        $widget->widget_name = $request->input('widget_name');
        $widget->save();

        // Now save Attributes...
        foreach($request->input('attributes') as $key => $value)
        {
            $attribute = WidgetAttribute::where('widget_id', $id)->where('widget_attribute_key', $key)->first();

            if(is_null($attribute))
            {
                flash('Widget Update Failed!')->error();
            }
            else
            {
                $update_attribute = WidgetAttribute::find($attribute->widget_attribute_id);
                $update_attribute->widget_attribute_key = $key; // May need to remove this...
                $update_attribute->widget_attribute_value = $value;
                $update_attribute->save();
            }
        }

        flash('Widget Updated!')->success();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
