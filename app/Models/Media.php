<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';
    protected $primaryKey = 'media_id';

    public function widget()
    {
        return $this->hasOne('App\Models\Widget', 'widget_id');
    }

    public function website()
    {
        return $this->hasOne('App\Models\Website', 'website_id');
    }
}
