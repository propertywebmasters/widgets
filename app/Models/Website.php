<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Website extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'website_id';
    protected $table = 'websites';

    public function widgets()
    {
        return $this->hasMany('App\Models\Widget', 'website_id');
    }

}
