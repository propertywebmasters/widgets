<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WidgetAttribute extends Model
{
    protected $primaryKey = 'widget_attribute_id';
    protected $table = 'widget_attributes';

    public function widget()
    {
        return $this->belongsTo('App\Models\Widget', 'widget_id', 'widget_id');
    }
}
