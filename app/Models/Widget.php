<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Dyrynda\Database\Support\GeneratesUuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Widget extends Model
{
    use GeneratesUuid;
    use SoftDeletes;

    protected $table = 'widgets';
    protected $primaryKey = 'widget_id';

    public function website()
    {
        return $this->hasOne('App\Models\Website', 'website_id', 'website_id');
    }

    public function attrs()
    {
        return $this->hasMany('App\Models\WidgetAttribute', 'widget_id', 'widget_id');
    }

    public function attributes_array()
    {
        //dd($this->attrs->pluck('widget_attribute_value', 'widget_attribute_key')->toArray());
        return $this->attrs->pluck('widget_attribute_value', 'widget_attribute_key')->toArray();
    }

    function getFriendlyDateAttribute()
    {
        return date("jS F Y", strtotime($this->created_at));
    }
}
