<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $primaryKey = 'company_id';

    public function users()
    {
        return $this->hasMany('App\Models\User', 'company_id', 'company_id');
    }

    public function websites()
    {
        return $this->hasManyThrough(
            'App\Models\Website',
            'App\Models\CompanyWebsite',
            'company_id', // Foreign key on company_website...
            'website_id', // Foreign key on websites table...
            'company_id', // Local key on company table...
            'website_id' // Local key on users table...
        );
    }
}
