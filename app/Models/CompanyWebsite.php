<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyWebsite extends Model
{
    protected $table = 'company_websites';
    protected $primaryKey = 'company_website_id';
}
