<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TestingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create Attributes...
        DB::table('widget_attributes')->insert(
            array(
                array(
                    'widget_id'                 => 1,
                    'widget_attribute_key'      => 'title',
                    'widget_attribute_value'    => 'Check out our Featured Property',
                    'created_at'                => \Carbon\Carbon::now()
                ),
                array(
                    'widget_id'                 => 1,
                    'widget_attribute_key'      => 'sub_title',
                    'widget_attribute_value'    => 'Fabulous 3 Bed Villa in the heart of Marbella',
                    'created_at'                => \Carbon\Carbon::now()
                ),
                array(
                    'widget_id'                 => 1,
                    'widget_attribute_key'      => 'main_text',
                    'widget_attribute_value'    => 'This beautiful property has just come on to the market and is priced to sell quickly, get in touch for more details and to book a viewing',
                    'created_at'                => \Carbon\Carbon::now()
                ),
                array(
                    'widget_id'                 => 1,
                    'widget_attribute_key'      => 'price',
                    'widget_attribute_value'    => '£350,000',
                    'created_at'                => \Carbon\Carbon::now()
                ),
                array(
                    'widget_id'                 => 1,
                    'widget_attribute_key'      => 'details_link',
                    'widget_attribute_value'    => 'https://www.bbc.co.uk',
                    'created_at'                => \Carbon\Carbon::now()
                )
            )
        );

    }
}
