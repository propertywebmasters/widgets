<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', '\App\Http\Controller\Auth\LoginController');

Route::get('/', function ()
{
    if(Auth::check())
    {
        return redirect('/admin');
    }
    else
    {
        return view('auth.login');
    }
});

// Auth
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

// Logged In (User)...
Route::group(['middleware' => ['auth']], function ()
{
    Route::group( ['prefix' => 'admin'], function()
    {
        Route::get('/', 'Admin\DashboardController@index');

        /*------------------------------------------------
         * Widget
         *------------------------------------------------ */
        Route::resource('widget', 'Admin\WidgetController');

        /*------------------------------------------------
         * Website
         *------------------------------------------------ */
         Route::resource('website', 'Admin\WebsiteController');
    });

});

//\Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query)
//{
//	echo "<pre>";
//	var_dump($query->sql);
//	var_dump($query->bindings);
//	var_dump($query->time);
//	echo "</pre>";
//});