$(document).ready(function()
{
    // Fade Out Alerts...
    $(".alert-dismissible").fadeTo(4000, 500).slideUp(500, function()
    {
        $(".alert-dismissible").alert('close');
    });

    /* TO DO -- REALLY NEED THIS IN VUE JS */
    $('.widget_type').on('change', function()
    {
        var type = $(this).val();

        $( ".types" ).each(function()
        {
            $(this).fadeOut();
        });

        $('#'+type).fadeIn("slow", function()
        {
            $('#'+type).attr("style", "display: block !important");
        });
    })

})