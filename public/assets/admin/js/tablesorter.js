(function($) {
  'use strict';
  $(function() {
    if ($('.table-sortable').length) {
      $('.table-sortable').tablesort();
    }
  });
})(jQuery);