/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/app.js":
/*!************************************!*\
  !*** ./resources/assets/js/app.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
//require('./bootstrap');
//window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
var app = new Vue({
  el: '#app'
});

/***/ }),

/***/ "./resources/assets/sass/styles.scss":
/*!*******************************************!*\
  !*** ./resources/assets/sass/styles.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

throw new Error("Module build failed (from ./node_modules/css-loader/index.js):\nModuleBuildError: Module build failed (from ./node_modules/sass-loader/dist/cjs.js):\n\r\n@import \"common\";\r\n       ^\r\n      Can't find stylesheet to import.\n   ╷\n82 │ @import \"common\";\n   │         ^^^^^^^^\n   ╵\n  stdin 82:9  root stylesheet\r\n      in C:\\xampp\\htdocs\\widgets\\resources\\assets\\sass\\styles.scss (line 82, column 9)\n    at runLoaders (C:\\xampp\\htdocs\\widgets\\node_modules\\webpack\\lib\\NormalModule.js:313:20)\n    at C:\\xampp\\htdocs\\widgets\\node_modules\\loader-runner\\lib\\LoaderRunner.js:367:11\n    at C:\\xampp\\htdocs\\widgets\\node_modules\\loader-runner\\lib\\LoaderRunner.js:233:18\n    at context.callback (C:\\xampp\\htdocs\\widgets\\node_modules\\loader-runner\\lib\\LoaderRunner.js:111:13)\n    at render (C:\\xampp\\htdocs\\widgets\\node_modules\\sass-loader\\dist\\index.js:89:7)\n    at Function.call$2 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:53451:16)\n    at _render_closure1.call$2 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:33267:12)\n    at _RootZone.runBinary$3$3 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:19664:18)\n    at _RootZone.runBinary$3 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:19668:19)\n    at _FutureListener.handleError$1 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18136:19)\n    at _Future__propagateToListeners_handleError.call$0 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18421:40)\n    at Object._Future__propagateToListeners (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:3470:88)\n    at _Future._completeError$2 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18257:9)\n    at _SyncCompleter._completeError$2 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18123:19)\n    at _SyncCompleter.completeError$2 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18092:12)\n    at _AsyncAwaitCompleter.completeError$2 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:17629:25)\n    at Object._asyncRethrow (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:3226:17)\n    at C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:10407:20\n    at _wrapJsFunctionForAsync_closure.$protected (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:3249:15)\n    at _wrapJsFunctionForAsync_closure.call$2 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:17664:12)\n    at _awaitOnObject_closure0.call$2 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:17656:25)\n    at _RootZone.runBinary$3$3 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:19664:18)\n    at _RootZone.runBinary$3 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:19668:19)\n    at _FutureListener.handleError$1 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18136:19)\n    at _Future__propagateToListeners_handleError.call$0 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18421:40)\n    at Object._Future__propagateToListeners (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:3470:88)\n    at _Future._completeError$2 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18257:9)\n    at _SyncCompleter._completeError$2 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18123:19)\n    at _SyncCompleter.completeError$2 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18092:12)\n    at Object.eval (eval at Closure_forwardCallTo (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:1515:14), <anonymous>:3:41)\n    at _RootZone.runBinary$3$3 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:19664:18)\n    at _RootZone.runBinary$3 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:19668:19)\n    at _FutureListener.handleError$1 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18136:19)\n    at _Future__propagateToListeners_handleError.call$0 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18421:40)\n    at Object._Future__propagateToListeners (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:3470:88)\n    at _Future._completeError$2 (C:\\xampp\\htdocs\\widgets\\node_modules\\sass\\sass.dart.js:18257:9)");

/***/ }),

/***/ 0:
/*!******************************************************************************!*\
  !*** multi ./resources/assets/js/app.js ./resources/assets/sass/styles.scss ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\xampp\htdocs\widgets\resources\assets\js\app.js */"./resources/assets/js/app.js");
module.exports = __webpack_require__(/*! C:\xampp\htdocs\widgets\resources\assets\sass\styles.scss */"./resources/assets/sass/styles.scss");


/***/ })

/******/ });