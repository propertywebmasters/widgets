@extends('layouts.auth')

@section('content')
<div class="col-lg-6 d-flex align-items-center justify-content-center">
    <div class="auth-form-transparent text-left p-3">
        <div class="brand-logo">
            <img src="{{ asset('assets/admin/images/logo.svg') }}" alt="logo">
        </div>
        <h4>Welcome back!</h4>
        <h6 class="font-weight-light">Please login below!</h6>
        <form method="POST" action="{{ route('login') }}" class="pt-3" novalidate="novalidate">
            @csrf
            <div class="form-group @error('email') has-error @enderror">
                <label for="exampleInputEmail">{{ __('E-Mail Address') }}</label>
                <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-account-outline text-primary"></i>
                      </span>
                    </div>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror form-control d-block form-control-lg border-left-0" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email Address">
                    @error('email')
                        <label id="cname-error" class="error mt-2 text-danger d-block w-100" for="cname">{{ $message }}</label>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword">{{ __('Password') }}</label>
                <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-lock-outline text-primary"></i>
                      </span>
                    </div>
                    <input id="password" type="password" class="form-control form-control-lg border-left-0 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                    @error('password')
                        <label id="cname-error" class="error mt-2 text-danger d-block w-100" for="cname">{{ $message }}</label>
                    @enderror
                </div>
            </div>
            <div class="my-3">
                <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">LOGIN</button>
            </div>
            <div class="text-center mt-4 font-weight-light">
                Don't have an account? <a href="#" class="text-primary font-weight-bold">Signup Now</a>
            </div>
        </>
    </div>
</div>
<div class="col-lg-6 login-half-bg d-flex flex-row">
    <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2018  All rights reserved.</p>
</div>
@endsection
