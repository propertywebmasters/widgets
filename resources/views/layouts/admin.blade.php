<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    @include('partials.admin.styles')
</head>

<body>
<input type="hidden" class="site_url" value="{{ url('/') }}">

<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    @include('partials.admin.nav')
    <div class="container-fluid page-body-wrapper">
        <!-- partial -->
        @include('partials.admin.sidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                @include('partials.admin.breadcrumbs')
                @include('flash::message')
                @yield('content')
            </div>
            @include('partials.admin.footer')
        </div>
        <!-- content-wrapper ends -->
    </div>
</div>

@include('partials.admin.scripts')

</body>
