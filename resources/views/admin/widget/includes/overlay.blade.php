<!-- Include File For Overlay Widget -->
@if($method == 'edit')
    @php
        $attributes = $widget->attributes_array();
    @endphp
@endif
<h4 class="card-title">@if(isset($type)) {{ ucwords($type) }} @else {{ ucwords($widget->widget_type) }} @endif Attributes</h4>
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label>Widget Title</label>
                <input name="attributes[title]" type="text" class="form-control" placeholder="Widget Title" value="@if($method == 'edit') @if(array_key_exists('title', $attributes)) {{ $attributes['title'] }}@endif @endif" required>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label>Widget Subtitle</label>
                <input name="attributes[sub_title]" type="text" class="form-control" placeholder="Widget Subtitle" value="@if($method == 'edit') @if(array_key_exists('sub_title', $attributes)) {{ $attributes['sub_title'] }}@endif @endif" required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Main Text</label>
            </div><!-- /.form-group -->
        </div><!-- /.col-md-12 -->
    </div><!-- /.row -->
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label>Price</label>
                    <div class="input-group flex-nowrap">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="addon-wrapping">&pound;</span>
                        </div>
                        <input name="attributes[price]" type="text" class="form-control" value="@if($method == 'edit') @if(array_key_exists('price', $attributes)) {{ $attributes['price'] }}@endif @endif" placeholder="Price" required>
                    </div>
            </div><!-- /.form-group -->
        </div><!-- /.col-md-6 -->
        <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label>Details Link</label>
                <input name="attributes[details_link]" type="text" class="form-control" value="@if($method == 'edit') @if(array_key_exists('details_link', $attributes)) {{ $attributes['details_link'] }}@endif @endif" placeholder="Details Link" required>
            </div><!-- /.form-group -->
            </div><!-- /.col-md-6 -->
    </div><!-- /.row -->