@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <form id="form" action="{{ url('admin/widget/'.$widget->widget_id) }}" method="post" data-toggle="validator">
                    @csrf
                    {{ method_field('PATCH') }}
                    <h4 class="card-title">Edit Widget - {{ $widget->widget_name }} <small>{{ $widget->widget_type }}</small></h4>
                    <p class="card-description">Edit your {{ $widget->widget_type }} widgets details below.</p>
                        <div class="form-group">
                            <label>Widget Name</label>
                            <input name="widget_name" type="text" class="form-control" placeholder="Widget Name" value="{{ $widget->widget_name }}" required>
                            <div class="invalid-feedback"></div>
                        </div>
                        <input type="hidden" name="website_id" value="{{ $widget->website_id }}">
                        <!-- e.g widget.includes.overlay.blade -->
                        @include('admin.widget.includes.'.$widget->widget_type)
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection