@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <form id="form" action="{{ url('admin/widget') }}" method="post" data-toggle="validator">
                        @csrf
                        <h4 class="card-title">Create Widget</h4>
                        <p class="card-description">Add your widgets details below.</p>
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label>Widget Name</label>
                                    <input name="widget_name" type="text" class="form-control" placeholder="Widget Name" value="{{ old('widget_name') }}" required>
                                    <div class="invalid-feedback"></div>
                                </div><!-- /.form-group -->
                            </div><!-- /.col-sm-12 -->
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label>Widget Type</label>
                                    <select class="form-control widget_type" name="widget_type" class="form-control">
                                        @foreach($types as $type)
                                            <option value="{{ $type }}">{{ $type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        @if($websites->count() == 1)
                            <input type="hidden" name="website_id" value="{{ $websites[0]->website_id }}">
                        @else
                            <select name="website_id">
                                @foreach($websites as $website)
                                    <option>{{ $website->website_name }}</option>
                                @endforeach
                            </select>
                        @endif
                        @foreach($types as $type)
                            <div id="{{ $type }}" class="d-none types">
                                @include('admin.widget.includes.'.$type)
                            </div>
                        @endforeach
                        <!-- e.g widget.includes.overlay.blade -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection