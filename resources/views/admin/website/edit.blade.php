@extends('layouts.admin')

@section('content')

    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <form id="form" action="{{ url('admin/website/'.$website->website_id) }}" method="post" data-toggle="validator">
                        @csrf
                        @method('PUT')
                        <h4 class="card-title">Edit Website - {{ $website->website_name }}</h4>
                        <p class="card-description">Edit your website details below.</p>
                        <div class="form-group @error('website_name') has-error has-danger @enderror">
                            <label>Website Name</label>
                            <input name="website_name" type="text" class="form-control" placeholder="Website Name" value="{{ $website->website_name }}" required>
                            @error('website_name')
                            <label id="cname-error" class="error mt-2 text-danger d-block w-100" for="cname">{{ $message }}</label>
                            @enderror
                        </div>
                        <div class="form-group @error('website_url') has-error has-danger @enderror">
                            <label>Website URL</label>
                            <input name="website_url" type="url" class="form-control" placeholder="Website URL" value="{{ $website->website_url }}">
                            @error('website_url')
                            <label id="cname-error" class="error mt-2 text-danger d-block w-100" for="cname">{{ $message }}</label>
                            @enderror
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection