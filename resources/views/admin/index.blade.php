@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Your Websites</h4>
                @if($company->websites->count() > 0)
                    @foreach($company->websites as $website)
                        <div class="border-bottom mt-4">
                            <div class="row">
                                <div class="col-sm-12 col-md-8">
                                    <h6 class="mb-1">{{ $website->website_name }}</h6>
                                    <small title="{{ count($website->widgets) }} - Widgets" class="text-muted mb-0"><i class="mdi mdi-terrain mr-1"></i>{{ count($website->widgets) }}</small>
                                </div><!-- /.col-md-8 -->
                                <div class="col-sm-12 col-md-4">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a class="btn btn-primary btn-sm" href="{{ url('admin/website/'.$website->website_id.'/edit') }}">Edit</a>
                                        <a class="btn btn-secondary btn-sm" href="#">Delete</a>
                                    </div><!-- /.btn-group -->
                                </div><!-- /.col-md-4 -->
                            </div><!-- /.row -->
                        </div><!-- /.border-bottom -->
                    @endforeach
                    <div class="text-center mt-4 mb-2">
                        <a class="btn btn-primary btn-sm" href="{{ url('admin/website/create') }}">Create New</a>
                    </div>
                @else
                    <div class="alert alert-info" role="alert">No websites at this time <strong><a href="#">Create a new Website</a></strong></div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title d-block">Your Widgets</h4>
                @if($widgets->count() > 0)
                    @foreach($widgets as $widget)
                        <div class="border-bottom mt-4">
                            <div class="row">
                                <div class="col-sm-12 col-md-8">
                                    <h6 class="mb-1">{{ $widget->widget_name }}</h6>
                                    <small class="text-muted mb-0"><i class="mdi mdi-earth mr-1"></i>{{ $widget->website->website_name }}</small>
                                </div><!-- /.col-md-8 -->
                                <div class="col-sm-12 col-md-4">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <a class="btn btn-primary btn-sm" href="{{ url('admin/widget/'.$widget->widget_id.'/edit') }}">Edit</a>
                                        <a class="btn btn-secondary btn-sm" href="#">Delete</a>
                                    </div><!-- /.btn-group -->
                                </div><!-- /.col-md-4 -->
                            </div><!-- /.row -->
                        </div><!-- /.border-bottom -->
                    @endforeach
                        <div class="text-center mt-4 mb-2">
                            <a class="btn btn-primary btn-sm" href="{{ url('admin/widget/create') }}">Create New</a>
                        </div>
                @else
                    <div class="alert alert-info" role="alert">No widgets at this time <strong><a href="{{ url('admin/widget/create') }}">Create a new Widget</a></strong></div>
                @endif
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{ asset('assets/admin/js/tablesorter.js') }}"></script>
@endpush

@endsection