<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="index.html">
                <i class="mdi mdi-view-quilt menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#dropdown" aria-expanded="false" aria-controls="ui-advanced">
                <i class="mdi mdi-layers menu-icon"></i>
                <span class="menu-title">Dropdown</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="dropdown">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="#">Item 1</a></li>
                    <li class="nav-item"> <a class="nav-link" href="#">Item 2</a></li>
                </ul>
            </div>
        </li>
    </ul>
</nav>