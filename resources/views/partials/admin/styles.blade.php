<!-- Styles -->
<link rel="stylesheet" href="{{ asset('assets/admin/vendor/iconfonts/mdi/font/css/materialdesignicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/vendor/css/vendor.bundle.base.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/vendor/css/vendor.bundle.addons.css') }}">
<link href="{{ asset('assets/admin/css/style.css') }}" rel="stylesheet" type="text/css">