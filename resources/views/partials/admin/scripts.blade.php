<!-- Scripts -->
<script src="{{ asset('assets/admin/vendor/js/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('assets/admin/vendor/js/vendor.bundle.addons.js') }}"></script>

<!-- Addons -->
<script src="{{ asset('assets/admin/js/script.js') }}"></script>
<script src="{{ asset('assets/admin/js/dashboard.js') }}"></script>
<script src="{{ asset('assets/admin/js/template.js') }}"></script>
<script src="{{ asset('assets/admin/js/off-canvas.js') }}"></script>
<script src="{{ asset('assets/js/validator.js') }}"></script>
@stack('scripts')