<nav class="bg-white" aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('admin') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Library</li>
    </ol>
</nav>