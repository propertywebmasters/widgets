<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright &copy; {{ date('Y') }} <a href="https://www.propertywebmasters.com.com/" target="_blank">Property Webmasters</a></span>
    </div>
</footer>